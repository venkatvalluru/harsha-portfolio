import React from 'react';
import logo from './logo.svg';
import './App.css';
//import Introduction from './components/Introduction';
import { BrowserRouter } from 'react-router-dom';
import Main from './components/Main';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Main />
      </div>
    </BrowserRouter>

  );
}

export default App;
