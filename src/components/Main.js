import React, { Component } from 'react';
import Academics from './Academics';
import Introduction from './Introduction';
import Header from './Header';
import Project from './Projects';
import Skills from './Skills';
import { Switch, Route, Redirect,withRouter} from 'react-router-dom';
class Main extends Component{
    render(){
        return(
            <div>
                <Header/>
                <Switch>
                    <Route path='/intro' component={Introduction}/>
                    <Route path='/academics' component={Academics}/>
                    <Route path='/projects' component={Project}/>
                    <Route path='/skills&experience' component={Skills}/>
                    <Redirect to="/intro" />
                </Switch>
            </div>
        )
    }
}
export default Main;