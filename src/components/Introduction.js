import React ,{Component} from 'react';
import {Jumbotron,Collapse,Navbar,NavbarToggler,NavbarBrand,Nav,NavItem,NavLink,Button,Carousel,CarouselItem,CarouselControl,CarouselIndicators,CarouselCaption,Media} from 'reactstrap';
const items=[
    {
        src:'assets/images/sundar.png',
        altText:'',
        caption:''  
    },
    {
        src:'assets/images/bill.png',
        altText:'',
        caption:''
    }
];

class Introduction extends Component{
    constructor(props){
        super(props);
        this.state = {
            activeIndex:0
        };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }
    onExiting(){
        this.animating = true;
    }
    
    onExited(){
        this.animating = false;
    }
    
    next(){
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }
    
    previous(){
        if (this.animating) return;
        const nextIndex=this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }
    
    goToIndex(newIndex){
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }
    render(){
        const {activeIndex} = this.state;

        const slides = items.map((item) => {
          return (
            <CarouselItem onExiting={this.onExiting} onExited={this.onExited} key={item.src}>
              <img src={item.src} alt={item.altText} className="d-block w-100 h-50" />
              <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
            </CarouselItem>
          );
        });
        return(
            <div>
                <div className="container" id="#container">
                    <div className="row row-content">
                        <div className="col">
                            <Carousel
                                activeIndex={activeIndex}
                                next={this.next}
                                previous={this.previous}
                            >
                                <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                                {slides}
                                <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
                                <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
                            </Carousel>
                        </div>
                    </div>
                    <div className="row row-content align-items-center">
                        <div className="col-3">
                            <img src="assets/images/harsha.jpg" width="350" height="350"/>
                        </div>
                        <div className="col-8 offset-1" id="justify">
                            <p>This is Venkat Sai Sriharsha Valluru currently pursuing Master's in Computer Science at the University of Oklahoma. I come from a town named Vijayawada located on the banks of river Krishna in AndhraPradesh,India. The three great pioneers that you see above are my inspirations. My idea is success has no shortcuts except hardwork.</p>
                            <p><b>"I have not failed. I've just found 10,000 ways that won't work"</b>.My journey in Web Development started in my Bachelor's where i was taught PHP,HTML,CSS. I didn't have any clue or experience on how to build websites. Then in my Master's when i was selected as Graduate Research Assistant i built some UI's using Python. Then its ReactJs that changed everything and brought enthusiasm in me. I am not a professional web designer but with React i am atleast putting my heart to build Front End UI's. </p><br></br>
                            <span className='fa fa-phone fa-lg'></span>:(+1)405-219-4071<br></br>
                            <a className="btn btn-social-icon btn-linkedin" href="http://www.linkedin.com/in/harsha-valluru/"><i className="fa fa-linkedin"></i></a>
                            <a className="btn btn-social-icon btn-microsoft" href="mailto:valluru@ou.edu"><i className="fa fa-envelope fa-lg"></i></a>
                            <a className="btn btn-social-icon btn-bitbucket" href="https://bitbucket.org/%7B5f3e168b-e293-4030-b00c-77a018d9040b%7D/"><i className='fa fa-bitbucket fa-sm'></i></a>
                            <a className="btn btn-social-icon btn-github" href="https://github.com/harshavalluru?tab=repositories"><i className='fa fa-github fa-sm'></i></a>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
} 

export default Introduction;