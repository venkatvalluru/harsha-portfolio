import React,{Component} from 'react';
import {Collapse,Button,CardBody,Card,CardImg} from 'reactstrap';
class Project extends Component{
    constructor(props){
        super(props)
        this.toggle = this.toggle.bind(this);
        this.toggle2 = this.toggle2.bind(this);
        this.toggle3= this.toggle3.bind(this);
        this.toggle4= this.toggle4.bind(this);
        this.toggle5= this.toggle5.bind(this);
        this.toggle6= this.toggle6.bind(this);
        this.state = {collapse:false,collapse2:false,collapse3:false,collapse4:false,collapse5:false,collapse6:false};
    }
    toggle() {
        this.setState(state => ({ collapse: !state.collapse }));
    }
    toggle2(){
        this.setState(state => ({ collapse2: !state.collapse2 }));
    }
    toggle3(){
        this.setState(state => ({ collapse3: !state.collapse3}));
    }
    toggle4(){
        this.setState(state => ({ collapse4: !state.collapse4})); 
    }
    toggle5(){
        this.setState(state => ({ collapse5: !state.collapse5})); 
    }
    toggle6(){
        this.setState(state => ({ collapse6: !state.collapse6})); 
    }
    render() {
        return (
          <div>
              <div className='container'>
                  <div className="row row-content">
                      <div className="col-12">
                        <Button color="primary" onClick={this.toggle} style={{ marginBottom: '1rem' }}>Redactor <span className="fa fa-chevron-down"></span></Button>
                        <Collapse isOpen={this.state.collapse}>
                            <Card>
                                <CardImg top width="100%" src="assets/images/redact.jpg" width="300" height="300" alt="Card image cap" />
                                <CardBody>
                                    <b>Overview</b>: Redacted sensitive data like names,gender,address,contact numbers in a set of documents using <b>Natural Language Processing</b>
                                    <p id="#justify"><b>Description</b>: Generally in a confidential document the sensitive data like names,gender,contact numbers,addresses have to be redacted.Thus,to achieve this I made use of Natural Language Processing that identfies a name,location and Porter Stemmer for stemming.To redact data like dates i made use of regular expressions.Spacy parser can be used to redact addresses.</p><br></br>
                                    <b>Languages</b>:Python
                                </CardBody>
                            </Card>
                        </Collapse>
                      </div>
                      <div className="col-12">
                        <Button color="primary" onClick={this.toggle2} style={{ marginBottom: '1rem' }}>Unredactor <span className="fa fa-chevron-down"></span></Button>
                        <Collapse isOpen={this.state.collapse2}>
                            <Card>
                                <CardImg top width="100%" src="assets/images/unredact1.jpg" width="300" height="300" alt="Card image cap" />
                                <CardBody>
                                    <b>Overview</b>: Predicted the redacted names,gender,address,contact numbers in a set of documents using <b>Machine Learning Algorithms</b>
                                </CardBody>
                            </Card>
                        </Collapse>
                      </div>
                      <div className="col-12">
                        <Button color="primary" onClick={this.toggle3} style={{ marginBottom: '1rem' }}>Search catalog <span className="fa fa-chevron-down"></span></Button>
                        <Collapse isOpen={this.state.collapse3}>
                            <Card>
                                <CardImg top width="100%" src="assets/images/search.jpeg" width="300" height="300" alt="Card image cap" />
                                <CardBody>
                                    <b>Overview</b>: Developed a search catalog that retrieves the information from a database that has 120K records using<b> PHP</b>
                                    <p><b>Description</b>:The Political Communication Center at the University of Oklahoma has metadata stored in a database that has 120k rows and 33 columns. The website of the Political Communication Center provides the user a functionality to retrieve a desired search from the database.The user is asked to fill in a form that has 7 fields and the results of the search are retrieved and displayed to the user.</p>
                                    <b>Languages</b>:PHP
                                </CardBody>
                            </Card>
                        </Collapse>
                      </div>
                    </div>
                    <div className="row row-content">
                            <div className="col-12">
                                <Button color="primary" onClick={this.toggle4} style={{ marginBottom: '1rem' }}>Restaurant cuisine <span className="fa fa-chevron-down"></span></Button>
                                <Collapse isOpen={this.state.collapse4}>
                                    <Card>
                                        <CardImg top width="100%" src="assets/images/menu.jpg" width="300" height="300" alt="Card image cap" />
                                        <CardBody>
                                            <b>Overview</b>: Developed a UI for a cuisine<b> ReactJs</b>
                                            <p><b>Description</b>:The UI for the cuisine consists of certain number of dishes that a user can select.The details of the dish are rendered if a particular dish is clicked.The user also has the flexibility to submit a feedback form about the dish.The project also has information about the featured dishes,chefs. </p>
                                            <b>Languages</b>:ReactJs <b>Frameworks</b>:Redux
                                        </CardBody>
                                    </Card>
                                </Collapse>
                            </div>
                            <div className="col-12">
                                <Button color="primary" onClick={this.toggle5} style={{ marginBottom: '1rem' }}>Metadata Downloader <span className="fa fa-chevron-down"></span></Button>
                                <Collapse isOpen={this.state.collapse5}>
                                    <Card>
                                        <CardImg top width="100%" src="assets/images/metadata.png" width="300" height="300" alt="Card image cap" />
                                        <CardBody>
                                            <b>Overview</b>: Developed a UI that downloads metadata from youtube using<b> Python Flask</b>
                                            <p><b>Description</b>:The Political Communication Center at the University of Oklahoma collects the metadata about various Political Commercials. All of the data is stored in an excel file and is uploaded to the database.The UI takes in the url of the videos in the youtube and produces an output in the form of an excel spreadsheet.</p>
                                            <b>Languages</b>:Python(Flask)
                                        </CardBody>
                                    </Card>
                                </Collapse>
                            </div>
                            <div className="col-12">
                                <Button color="primary" onClick={this.toggle6} style={{ marginBottom: '1rem' }}>Tree Crown Delineation <span className="fa fa-chevron-down"></span></Button>
                                <Collapse isOpen={this.state.collapse6}>
                                    <Card>
                                        <CardImg top width="100%" src="assets/images/cartosat.jpg" width="300" height="300" alt="Card image cap" />
                                        <CardBody>
                                            <b>Overview</b>: Implemented algorithms that enhance the clarity of a cartosat image using<b> Java</b>
                                            <p><b>Description</b>:The tree crowns in a cartosat images are identified in three stages.First the image is given as an input to one of the algorithms that are a part of Image Enhancement.The output generated in the first stage is sent as an input to the second stage i.e Image Segementation and finally the output generated in this stage is sent to the final stage i.e Feature Extraction.Thus,tree crowns are identified.</p> 
                                            <b>Languages</b>:Java
                                        </CardBody>
                                    </Card>
                                </Collapse>
                            </div>
                    </div>
              </div>
          </div>
        );
    }
}
export default Project