import React,{Component} from 'react';
import {Card,CardImg,CardText,CardBody,CardTitle,CardSubtitle} from 'reactstrap';
class Skills extends Component{
    render(){
        const responsibilities=['Developed and maintained python scripts that download metadata of Political Commercials from Youtube,that covert an excel file to yale template','Developed and maintained the website of the Political Communication Center at the University of Oklahoma','Configured and maintained the database of the Political Communication Center ']
        const items=responsibilities.map((responsibility)=><li>{responsibility}</li>)
        const duties=['Implemented algorithms in Java that identify tree crowns using cartosat images.']
        const numbers=duties.map((duty)=><li>{duty}</li>)
        const certifications=["Front-End Web UI Frameworks and Tools: Bootstrap4 by The Hong Kong University of Science and Technology on Coursera. Certificate earned at Wednesday, July 10, 2019 6:00 AM GMT","Front-End Web Development with React by The Hong Kong University of Science and Technology on Coursera. Certificate earned at Sunday, August 11, 2019 1:08 AM GMT","Version control:Git","Programming Languages:Python,Java,PHP,ES6,HTML,CSS","Frameworks and Tools:React,Redux,MySQL,MATLAB"]
        const certificates=certifications.map((certification)=><li>{certification}</li>)
        return(
            <div className='container'>
                <div className="row row-content">
                    <div className='col-12'>
                        <h3>Professional Experience</h3>
                        <Card>
                            <CardBody>
                                <CardTitle><h4>Graduate Research Assistant</h4></CardTitle>
                                <CardSubtitle><b>Web Developer,Python Developer,DBAdmin</b></CardSubtitle>
                                <CardText><ul>{items}</ul></CardText>
                            </CardBody>
                        </Card>
                    </div>
                    <div className='col-12'>
                        <Card>
                            <CardBody>
                                <CardTitle><h4> Research Assistant</h4></CardTitle>
                                <CardSubtitle><b>Java Developer</b></CardSubtitle>
                                <CardText><ul>{numbers}</ul></CardText>
                            </CardBody>
                        </Card>     
                    </div>
                </div>
                <div className='row row-content'>
                    <div className='col-12'>
                        <h3>Skills and Certifications:</h3>
                        <ul>{certificates}</ul>
                    </div>
                </div>
            </div>
        )
    }
}
export default Skills;