import React, { Component } from 'react';
import { Navbar, NavbarBrand, Jumbotron ,Nav,NavbarToggler,Collapse,NavItem,Button,Media} from 'reactstrap';
import {NavLink} from 'react-router-dom';
class Header extends Component{
    constructor(props){
        super(props)
        this.toggleNav = this.toggleNav.bind(this);
        this.state = {
            isNavOpen: false
        }
    }
    toggleNav(){
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }
    render(){
        return(
            <div>
                <Navbar dark expand="md">
                    <div className='container'>
                        <NavbarToggler onClick={this.toggleNav} />
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                                        <Nav navbar>
                                        <NavItem>
                                            <NavLink className="nav-link active" to='/intro'><span className="fa fa-home fa-md"></span>Home</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink className="nav-link" to='/academics'><span className="fa fa-university fa-md"></span>Acadamics</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink className="nav-link" to='/projects'><span className="fa fa-tasks fa-md"></span>Projects</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink className="nav-link" to='/skills&experience' ><span className="fa fa-address-card fa-md"></span>Skills&Experience</NavLink>
                                        </NavItem>
                                        </Nav>
                                        <Nav className="ml-auto" navbar>
                                            <NavItem>
                                                <Button outline onClick={this.toggleModal}><span className="fa fa-sign-in fa-md"></span>Login</Button>
                                            </NavItem>
                                        </Nav>
                        </Collapse>
                    </div>
                </Navbar>
                <Jumbotron>
                    <div className='container'>
                        <div className='row row-header'>
                            <div className='col-12 col-sm-12'>
                                <Media>
                                    <Media body>
                                        <Media heading>
                                            Passion+HardWork=Success
                                        </Media>
                                        I am a dedicated and hardworking person always looking forward to learn new things. Along with my enthusiasm to learn, I strongly believe I possess analytical and technical skills required to complete the tasks that I take up.
                                    </Media>
                                </Media>
                            </div>
                        </div>
                    </div>
                </Jumbotron> 
            </div>
        )
    }
}

export default Header;