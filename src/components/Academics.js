import React,{Component} from 'react';
import {Card,CardImg,CardText,CardBody,CardTitle,CardSubtitle} from 'reactstrap';
class Academics extends Component{
    render(){
        return(
            <div className="container" id="bg">
                <div className="row row-content align-items-center" >
                    <div className="col-3">
                        <img src="assets/images/ou.jpeg" width="350" height="350"/>
                    </div>
                    <div className="col-8 offset-1" id="justify">
                        <h3>The University of Oklahoma,Norman</h3>
                        <p><b>Start of graduation</b>:August 20,2018</p>
                        <p>GPA:3.667</p>
                        <p>Currently pursuing Master's in Computer Science at The University of Oklahoma.</p>
                        <p><b>Expected time of graduation</b>:May,2020</p>
                     </div>
                </div>
                <div className="row row-content align-items-center" >
                    <div className="col-3">
                        <img src="assets/images/vrsec.jpeg" width="350" height="350"/>
                    </div>
                    <div className="col-8 offset-1" id="justify">
                        <h3>VR Siddhartha College of Engineering,Vijayawada</h3>
                        <p><b>Start of graduation</b>:September,2014</p>
                        <p>GPA:3.4</p>
                        <p>Completed Bachelor's from VRSEC with a GPA of 3.4 in Computer Science.</p>
                        <p><b>Date of graduation</b>:April,2018</p>
                    </div>
                </div>
            </div>
        )
    }
}
export default Academics;